package exc.java;

public class Ex {

			   public static void main(String args[]) {
			     try { 
			          System.out.println("Try block message");
			     } catch (Exception e) {
			            System.out.println("Error: Don't divide a number by zero");
			       }
			     System.out.println("I'm out of try-catch block in Java.");
			   }
			

	}

